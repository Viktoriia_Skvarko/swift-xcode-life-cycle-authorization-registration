//  RegistrationVC.swift
//  AutorizationAndRegistration
//  Created by Viktoriia Skvarko


import UIKit

class RegistrationVC: UIViewController {
    
    @IBOutlet weak var podlozkaRegist: UIView!
    
    @IBOutlet weak var usernameRegistration: UITextField!
    @IBOutlet weak var emailRegistration: UITextField!
    @IBOutlet weak var phoneRegistration: UITextField!
    @IBOutlet weak var passwordRegistration: UITextField!
    @IBOutlet weak var confirmPasswordRegistration: UITextField!
    
    @IBAction func chekBoxAcseprRegisrAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBOutlet weak var buttonAcceptRegistration: UIButton!
    
    @IBAction func singInButtonRegistrationAction(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AutorizVC") {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonAcceptRegistration.setTitleColor(UIColor(red: 0.2, green: 0.4, blue: 0.7, alpha: 1) , for: .normal)
        
        podlozkaRegist.layer.cornerRadius = 4
        podlozkaRegist.layer.borderWidth = 1
        podlozkaRegist.layer.borderColor = UIColor.white.cgColor
        
        usernameRegistration.borderStyle = .none
        usernameRegistration.backgroundColor = UIColor.white
        
        emailRegistration.borderStyle = .none
        emailRegistration.backgroundColor = UIColor.white
        
        phoneRegistration.borderStyle = .none
        phoneRegistration.backgroundColor = UIColor.white
        
        passwordRegistration.borderStyle = .none
        passwordRegistration.backgroundColor = UIColor.white
        
        confirmPasswordRegistration.borderStyle = .none
        confirmPasswordRegistration.backgroundColor = UIColor.white
        
    }
    
}
