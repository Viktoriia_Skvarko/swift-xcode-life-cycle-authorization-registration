//  ForgotPasswordVC.swift
//  AutorizationAndRegistration
//  Created by Viktoriia Skvarko


import UIKit

class ForgotPasswordVC: UIViewController {
    
    var password: String?
    
    @IBOutlet weak var podlozkaForgotPass: UIView!
    
    @IBOutlet weak var codeFromSms: UITextField!
    
    @IBOutlet weak var emailForgotPassw: UITextField!
    
    
    @IBAction func singInButtonInPageFogot(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AutorizVC") {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func regestrationButtonInPageFogot(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "RegistrationVC") {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailForgotPassw.text = password
        
        podlozkaForgotPass.layer.cornerRadius = 4
        podlozkaForgotPass.layer.borderWidth = 1
        podlozkaForgotPass.layer.borderColor = UIColor.white.cgColor
        
        codeFromSms.borderStyle = .none
        codeFromSms.backgroundColor = UIColor.white
        
        emailForgotPassw.borderStyle = .none
        emailForgotPassw.backgroundColor = UIColor.white
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changetPassword.password = emailForgotPassw.text ?? ""
        
        print("Окно восстановления пароля сейчас закроется")
    }
    
}
