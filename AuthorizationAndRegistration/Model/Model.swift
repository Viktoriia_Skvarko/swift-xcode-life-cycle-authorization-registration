//  Model.swift
//  AutorizationAndRegistration
//  Created by Viktoriia Skvarko


import Foundation

protocol ChangePasswordDelegate {
    func changePasswordDidUpdate(_ password: String)
}

struct ChangePassword {
    
    var delegate: ChangePasswordDelegate?
    
    var password: String {
        didSet {
            delegate?.changePasswordDidUpdate(password)
        }
    }
    
}
