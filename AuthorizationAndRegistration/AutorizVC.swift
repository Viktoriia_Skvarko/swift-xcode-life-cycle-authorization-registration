//  AutorizVC.swift
//  AutorizationAndRegistration
//  Created by Viktoriia Skvarko


import UIKit

class AutorizVC: UIViewController {
    
    @IBOutlet weak var podlozkaAutoriz: UIView!
    
    @IBOutlet weak var rememberMeButton: UIButton!
    
    @IBOutlet weak var emailTextFieldAutoriz: UITextField!
    @IBOutlet weak var passwordTextFieldAutoriz: UITextField!
    
    
    @IBAction func chekBoxButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewWillAppear(_ animated: Bool) {
        print("Первый экран приложения сейчас откроется")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.changetPassword.delegate = self
        
        
        passwordTextFieldAutoriz.text = appDelegate.changetPassword.password
        
        
        rememberMeButton.setTitleColor(UIColor(red: 0.2, green: 0.4, blue: 0.7, alpha: 1) , for: .normal)
        
        podlozkaAutoriz.layer.cornerRadius = 4
        podlozkaAutoriz.layer.borderWidth = 1
        podlozkaAutoriz.layer.borderColor = UIColor.white.cgColor
        
        emailTextFieldAutoriz.borderStyle = .none
        emailTextFieldAutoriz.backgroundColor = UIColor.white
        
        passwordTextFieldAutoriz.borderStyle = .none
        passwordTextFieldAutoriz.backgroundColor = UIColor.white
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ForgotPasswordVC {
            vc.password = appDelegate.changetPassword.password
        }
    }
    
}


extension AutorizVC: ChangePasswordDelegate {
    func changePasswordDidUpdate(_ password: String) {
        passwordTextFieldAutoriz.text = password
    }
}
